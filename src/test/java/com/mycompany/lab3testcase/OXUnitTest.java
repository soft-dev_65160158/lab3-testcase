/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3testcase;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author armme
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinRow1_O_output_true(){
        String[][] table = {{"o","o","o"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWinRow2_O_output_true(){
        String[][] table = {{"-","-","-"},{"o","o","o"},{"-","-","-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWinRow3_O_output_true() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"o", "o", "o"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinCol1_O_output_true() {
        String table[][] = {{"o", "-", "-"}, {"o", "-", "-"}, {"o", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol2_O_output_true() {
        String table[][] = {{"-", "o", "-"}, {"-", "o", "-"}, {"-", "o", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol3_O_output_true() {
        String table[][] = {{"-", "-", "o"}, {"-", "-", "o"}, {"-", "-", "o"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinRow1_X_output_true(){
        String[][] table = {{"x","x","x"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWinRow2_X_output_true(){
        String[][] table = {{"-","-","-"},{"x","x","x"},{"-","-","-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWinRow3_X_output_true() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"x","x","x"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinCol1_X_output_true() {
        String table[][] = {{"x", "-", "-"}, {"x", "-", "-"}, {"x", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol2_X_output_true() {
        String table[][] = {{"-","x", "-"}, {"-", "x", "-"}, {"-", "x", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol3_X_output_true() {
        String table[][] = {{"-", "-", "x"}, {"-", "-", "x"}, {"-", "-", "x"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinRow1_O_1_output_false() {
        String table[][] = {{"-", "o", "o"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow1_O_2_output_false() {
        String table[][] = {{"O", "-", "o"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow1_O_3_output_false() {
        String table[][] = {{"o", "o", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow2_O_1_output_false() {
        String table[][] = {{"-", "-", "-"}, {"-", "o", "o"}, {"-", "-", ""}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow2_O_2_output_false() {
        String table[][] = {{"-", "-", "-"}, {"o", "-", "o"}, {"-", "-", ""}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow2_O_3_output_false() {
        String table[][] = {{"-", "-", "-"}, {"o", "o", "-"}, {"-", "-", ""}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow3_O_1_output_false() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "o", "o"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow3_O_2_output_false() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"o", "-", "o"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow3_O_3_output_false() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"o", "o", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol1_O_1_output_false() {
        String table[][] = {{"-", "-", "-"}, {"o", "-", "-"}, {"o", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol1_O_2_output_false() {
        String table[][] = {{"o", "-", "-"}, {"-", "-", "-"}, {"o", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol1_O_3_output_false() {
        String table[][] = {{"o", "-", "-"}, {"o", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol2_O_1_output_false() {
        String table[][] = {{"-", "-", "-"}, {"-", "o", "-"}, {"-", "o", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol2_O_2_output_false() {
        String table[][] = {{"-", "o", "-"}, {"-", "-", "-"}, {"-", "O", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol2_O_3_output_false() {
        String table[][] = {{"-", "O", "-"}, {"-", "o", "-"}, {"-", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_O_1_output_false() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "o"}, {"-", "-", "o"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_O_2_output_false() {
        String table[][] = {{"-", "-", "o"}, {"-", "-", "-"}, {"-", "-", "O"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_O_3_output_false() {
        String table[][] = {{"-", "-", "o"}, {"-", "-", "o"}, {"-", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinRow1_X_1_output_false() {
        String table[][] = {{"-", "x", "x"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow1_X_2_output_false() {
        String table[][] = {{"x", "-", "x"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow1_X_3_output_false() {
        String table[][] = {{"x", "x", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow2_X_1_output_false() {
        String table[][] = {{"-", "-", "-"}, {"-", "x", "x"}, {"-", "-", ""}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow2_X_2_output_false() {
        String table[][] = {{"-", "-", "-"}, {"x", "-", "x"}, {"-", "-", ""}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow2_X_3_output_false() {
        String table[][] = {{"-", "-", "-"}, {"x", "x", "-"}, {"-", "-", ""}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow3_X_1_output_false() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "x", "x"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow3_X_2_output_false() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"x", "-", "x"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow3_X_3_output_false() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"x", "x", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol1_X_1_output_false() {
        String table[][] = {{"-", "-", "-"}, {"x", "-", "-"}, {"x", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol1_X_2_output_false() {
        String table[][] = {{"x", "-", "-"}, {"-", "-", "-"}, {"x", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol1_X_3_output_false() {
        String table[][] = {{"x", "-", "-"}, {"x", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol2_X_1_output_false() {
        String table[][] = {{"-", "-", "-"}, {"-", "x", "-"}, {"-", "x", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol2_X_2_output_false() {
        String table[][] = {{"-", "x", "-"}, {"-", "-", "-"}, {"-", "x", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol2_X_3_output_false() {
        String table[][] = {{"-", "x", "-"}, {"-", "x", "-"}, {"-", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_X_1_output_false() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "x"}, {"-", "-", "x"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_X_2_output_false() {
        String table[][] = {{"-", "-", "x"}, {"-", "-", "-"}, {"-", "-", "x"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_X_3_output_false() {
        String table[][] = {{"-", "-", "x"}, {"-", "-", "x"}, {"-", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinX1_O_output_True() {
        String table[][] = {{"o", "-", "-"}, {"-", "o", "-"}, {"-", "-", "o"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinX1_X_output_True() {
        String table[][] = {{"x", "-", "-"}, {"-", "x", "-"}, {"-", "-", "x"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinX2_Left_O_output_True() {
        String table[][] = {{"-", "-", "o"}, {"-", "o", "-"}, {"o", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void ttestCheckWinX2_LeftX_output_True() {
        String table[][] = {{"-", "-", "x"}, {"-", "x", "-"}, {"x", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinX1_O_1_1output_False() {
        String table[][] = {{"-", "-", "-"}, {"-", "o", "-"}, {"-", "-", "o"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinX1_O_2_2output_False() {
        String table[][] = {{"o", "-", "-"}, {"-", "-", "-"}, {"-", "-", "o"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinX1_O_3_3output_False() {
        String table[][] = {{"o", "-", "-"}, {"-", "o", "-"}, {"-", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinX1_X_1_1output_False() {
        String table[][] = {{"-", "-", "-"}, {"-", "x", "-"}, {"-", "-", "x"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinX1_X_2_2output_False() {
        String table[][] = {{"x", "-", "-"}, {"-", "-", "-"}, {"-", "-", "x"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinX1_X_3_3output_False() {
        String table[][] = {{"x", "-", "-"}, {"-", "x", "-"}, {"-", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinX2_O_1_3output_False() {
        String table[][] = {{"-", "-", "-"}, {"-", "O", "-"}, {"o", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinX2_O_2_2output_False() {
        String table[][] = {{"-", "-", "o"}, {"-", "-", "-"}, {"o", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinX2_O_3_1output_False() {
        String table[][] = {{"-", "-", "o"}, {"-", "o", "-"}, {"-", "-", "-"}};
        String currentPlayer = "o";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinX2_X_1_3output_False() {
        String table[][] = {{"-", "-", "-"}, {"-", "x", "-"}, {"x", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinX2_X_2_2output_False() {
        String table[][] = {{"-", "-", "x"}, {"-", "-", "-"}, {"x", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinX2_X_3_1output_False() {
        String table[][] = {{"-", "-", "x"}, {"-", "x", "-"}, {"-", "-", "-"}};
        String currentPlayer = "x";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    


    
    

    
}
